/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
                          
*/

	function register(username) {

		/* - determine if the input username already exists in our registeredUsers array.
            - if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"*/
		if (registeredUsers.includes(username) === true) {
			alert("Registration failed. Username already exists!");
		}

        else if (username === undefined) {
            alert("Invalid!");
        }
        
        // -if it is not, add the new username into the registeredUsers array and show an alert:
                // "Thank you for registering!"
        else {
            registeredUsers.push(username);
            alert("Thank you for registering!");
        };
		
	};
    
        // - invoke and register a new user.
        // - outside the function log the registeredUsers array.
	

/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.            
        

*/

    function addFriend(username) {
        /*  - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
            -Then show an alert with the following message:
                "You have added <registeredUser> as a friend!"*/        
        if (registeredUsers.includes(username) === true && friendsList.includes(username) === false) {
        friendsList.push(username); 
        alert("You have added " + username + " as a friend!"); 

        /*if it is not, show an alert window with the following message:
                "User not found."*/
        } else if (friendsList.includes(username) === true) {
            alert(username + " is already your friend!"); 

        }
        else if (username === undefined) {
            alert("Invalid!");
        } 
        else {
            alert("User not found.");
        };

    };

        /*  - invoke the function and add a registered user in your friendsList.
            - Outside the function log the friendsList array in the console.*/

/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        

*/
    function displayFriends(){
        friendsList.forEach(function(friend){
            console.log(friend)
        })

        /*- If the friendsList is empty show an alert: 
            "You currently have 0 friends. Add one first."*/
        if (friendsList.length === 0) {
            alert("You currently have 0 friends. Add one first.");
        }
    };
        // - Invoke the function.
    

/*
    4. Create a function which will display the amount of registered users in your friendsList.

*/
    function displayNumberOfFriends() {
        let numberOfFriends = friendsList.length;

        /* - If the friendsList is empty show an alert:
                "You currently have 0 friends. Add one first."*/
        if (numberOfFriends === 0) {
            alert("You currently have 0 friends. Add one first.");
        }

        /*  - If the friendsList is not empty show an alert:
                "You currently have <numberOfFriends> friends."*/
        else if (numberOfFriends === 1){
            alert("You currently have "+ numberOfFriends + " friend.");
        }
        else {
            alert("You currently have "+ numberOfFriends + " friends.");
        };
    };
        
        // - Invoke the function

/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        

*/

    function deleteFriend() {
        
        if(friendsList.length > 0) {
            friendsList.pop();
        }

        /* - If the friendsList is empty show an alert:
                "You currently have 0 friends. Add one first."*/
        else {
            alert("You currently have 0 friends. Add one first.");
        };
    };
        /*  - Invoke the function.
            - Outside the function log the friendsList array.*/


/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/

    function deleteSpecificFriend(username) {
        if (friendsList.includes(username) === true) {
            let index = friendsList.indexOf(username);
            let deleteCount = arguments.length;            
        friendsList.splice(index, deleteCount);
        }

        else {
            alert(username + " is not your friend.")
        };        
    };



